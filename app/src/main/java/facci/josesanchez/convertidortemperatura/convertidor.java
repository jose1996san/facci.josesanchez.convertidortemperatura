package facci.josesanchez.convertidortemperatura;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class convertidor extends Activity {

    EditText EditTextcantidad;
    Button btnCAF, btnFAC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor);
        EditTextcantidad = (EditText) findViewById(R.id.EditTextcantidad);
        btnCAF = (Button) findViewById(R.id.btnCAF);
        btnFAC = (Button) findViewById(R.id.btnFAC);


        btnCAF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double cantidad = Double.parseDouble(EditTextcantidad.getText().toString());
                float resultado = (float) ((cantidad * 1.8) + 32);
                Toast.makeText(getApplicationContext(), "El resultado de la conversión es:" + resultado+"°F", Toast.LENGTH_LONG).show();

            }
        });
        btnFAC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double cantidad = Double.parseDouble(EditTextcantidad.getText().toString());
                float resultado = (float) ((cantidad - 32)/(1.8));
                Toast.makeText(getApplicationContext(), "El resultado de la conversión es:" + resultado+"°C", Toast.LENGTH_LONG).show();
            }
        });
    }
}
